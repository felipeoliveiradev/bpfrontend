const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require("webpack-merge");
const { DefinePlugin } = require('webpack')
const common = require("./webpack.common");

module.exports = merge(common, {
    mode: "production",
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    plugins: [
        new DefinePlugin({
            "process.env.API_URL": JSON.stringify(
                "https://brasilparalelo.tk/api"
            ),
        }),
        new HtmlWebpackPlugin({
            template: "./template.prod.html",
        }),
    ],
});
