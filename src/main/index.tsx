import { Presentation } from 'presentation/index'
import { light } from 'presentation/packages/ui/flupper/styles/themes/light/light'
import React from 'react'
import ReactDOM from 'react-dom'
import { SkeletonTheme } from 'react-loading-skeleton'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle } from './config/styled/style'
import { AuthProvider } from './contexts'

ReactDOM.render(
    <>
        <ThemeProvider theme={light}>
            <BrowserRouter>
                <SkeletonTheme baseColor="#202020" highlightColor="#131313">
                    <AuthProvider>
                        <Presentation />
                    </AuthProvider>
                </SkeletonTheme>
                <GlobalStyle />
            </BrowserRouter>
        </ThemeProvider>
    </>,
    document.getElementById('root')
)
