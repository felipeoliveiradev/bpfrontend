import Http, { HttpResponse } from 'infra/http'
import { IHttpOutput } from 'infra/http/interface-http'

export class UserContextRequest {
  async login (token: string): Promise<IHttpOutput<any>> {
    const result = await Http.get<any>('/authentication', {
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
    return HttpResponse<any>(result)
  }
}
