import { IHttpOutput } from 'infra/http/interface-http'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { UserContextRequest } from './user-context-request'

const AuthContext = React.createContext(null)
const requests = new UserContextRequest()
const auth = async (tokens: string): Promise<IHttpOutput<any>> => {
  return await requests.login(tokens)
}
type AuthProviderType = {
  children: JSX.Element
}

const AuthProvider = ({ children }: AuthProviderType): JSX.Element => {
  const session = sessionStorage.getItem('token')
  const sessionUser = JSON.parse(sessionStorage.getItem('user'))

  const [token, setToken] = React.useState(session || null)
  const [user, setUser] = React.useState(sessionUser?.image || '')
  const navigate = useNavigate()

  const handleLogin = async (tokens: string): Promise<void> => {
    if (tokens) {
      const token = await auth(tokens)
      if (token.status) {
        setToken(tokens)
        navigate('/browse')
      } else {
        setToken(null)
        navigate('/login')
      }
    } else if (session) {
      const token = await auth(tokens)
      if (token.status) {
        setToken(tokens)
        navigate('/browse')
      } else {
        setToken(null)
        navigate('/login')
      }
    } else {
      setToken(null)
      navigate('/login')
    }
  }

  const handlePicture = (image: string): void => {
    if (image !== user) {
      setUser(image)
    }
  }
  const handleLogout = (): void => {
    setToken(null)
  }

  const value = {
    token,
    user,
    onPicture: handlePicture,
    onLogin: handleLogin,
    onLogout: handleLogout
  }

  return (
      <AuthContext.Provider value={value}>
        {children}
      </AuthContext.Provider>
  )
}
const useAuth = (): any => {
  return React.useContext(AuthContext)
}
export { useAuth, AuthProvider }
