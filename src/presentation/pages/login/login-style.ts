import styled from 'styled-components'

export const Container = styled.div`
    color: #ffffff;
    display: grid;
    height: 100%;
    grid-template-columns: 2fr 1fr;
    grid-template-rows: auto;
    grid-template-areas: "highlights infos";
`

export const Highlights = styled.div`
    grid-area: highlights;
    background: url("https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/92356084213065.5f952b343a40c.png");
    background-size: cover;
    display: flex;
    flex-direction: column;
    justify-content: center;
    min-width: 500px;
    padding: 2rem;
    position: relative;
    h1 {
        background-color: transparent;
        color: #ffffff;
        font-weight: 400;
        font-size: 35px;
        margin-bottom: 0.6rem;
        letter-spacing: 0;
        z-index: 1;
    }
    h2 {
        background-color: transparent;
        color: #b4b8ce;
        font-weight: 400;
        font-size: 20px;
        margin-bottom: 1.3rem;
        letter-spacing: 0;
        z-index: 1;
    }
    p {
        background-color: transparent;
        color: #ffffff;
        font-weight: 200;
        max-width: 500px;
        line-height: 1.4rem;
        text-align: justify;
        z-index: 1;
    }

    &:before {
        content: "";
        position: absolute;
        width: 100%;
        height: 100%;
        /* background-color: rgba(0, 0, 0, 0.7); */
        left: 0;
        z-index: 0;
    }
`

export const Infos = styled.div`
    grid-area: infos;
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    box-shadow: -6px 0px 18px -8px rgba(217, 217, 217, 1);
    z-index: 1;
    h1 {
        font-size: 2rem;
        font-weight: 500;
        color: #6d6e70;
        margin-bottom: 0.5rem;
    }
    p {
        font-size: 1rem;
        font-weight: 500;
        color: #cecece;
        margin-bottom: 2rem;
    }
    form {
        display: flex;
        flex-direction: column;
        width: 100%;
        max-width: 500px;
        gap: 20px;
    }
`
