export interface ILoginState {
  loading: {
    setLoading: any
    loading: any
  }
  error: {
    setError: any
    error: any
  }
  context: {
    token: any
    onLogin: any
  }
}
export interface ILoginValue {
  username: string
  password: string
}
export interface ILoginResult {
  username: string
  name: string
  lastname: string
  role: string
  email: string
  cellphone: string
  token: string
}
