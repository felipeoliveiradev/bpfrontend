import React, { useState } from 'react'
import { Layout } from 'presentation/packages'
import { layout } from './login-layout'
import { ILoginState } from './login-interface'
import { useNavigate } from 'react-router-dom'
import { useAuth } from 'main/contexts'

export const Login: React.FC = () => {
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState()
  const { token, onLogin } = useAuth()
  const history = useNavigate()
  const states: ILoginState = {
    loading: { loading, setLoading },
    error: { error, setError },
    context: { token, onLogin }
  }
  return (
        <Layout
            {...layout({
              state: states,
              history
            })}
        />
  )
}
