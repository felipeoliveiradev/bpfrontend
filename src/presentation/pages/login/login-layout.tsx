import { IComponents } from 'infra/types'
import { Loading } from 'presentation/components'
import { Button, Column, Container, Form, Line, pxToRem, Spacer, Text } from 'presentation/packages'
import { ETextType } from 'presentation/packages/ui/flupper/Text/interfaces'
import { LoginController } from './login-controller'
import { ILoginState, ILoginValue } from './login-interface'

export const layout = (props: IComponents<ILoginValue, ILoginState>): object => {
  const { state, history } = props
  const controller = new LoginController(state, history)

  return {
    props: {
      style: { height: '100%' }
    },
    children: Container({
      children: [
        Line({
          props: {
            flex: 1,
            styles: {
              tablet: {
                flex: 1,
                flexDirection: 'column'
              }
            }
          },
          children: [
            Column({
              props: {
                style: {
                  background: 'linear-gradient(90deg, rgba(2,0,36,0.23433123249299714) 0%, rgba(0,0,0,1) 99%), url("https://assets-global.website-files.com/60ff690cd7b0537edb99a29a/61e1df8a1ec0a414c2ef0c91_bp-select.png") center /cover'
                },
                centered: 'all',
                styles: {
                  tablet: {
                    flex: 2
                  }
                }
              },
              children: [
                Container({
                  props: {
                    centered: 'all',
                    styles: {
                      desktop: {
                        padding: '1rem'
                      },
                      tablet: {
                        alignItems: 'flex-start',
                        padding: '2rem'
                      }
                    }
                  },
                  children: []
                })
              ]
            }),
            Column({
              props: {
                size: 'medium',
                centered: 'all',
                style: { backgroundColor: '#000', boxShadow: '-6px 0px 20px -8px #000000', zIndex: 1 },
                styles: {
                  tablet: {
                    flex: 1,
                    padding: '2.5rem 2rem'
                  }
                }
              },
              children: [
                state.loading.loading
                  ? Column({
                    children: [
                      Text({
                        text: 'Brasil Paralelo',
                        type: ETextType.h3,
                        props: { style: { fontWeight: 500 } }
                      }),
                      Spacer({}),
                      Text({
                        text: 'Depois que ver não tem como voltar',
                        type: ETextType.p,
                        props: { style: { fontWeight: 500 } }
                      }),
                      Spacer({}),
                      Text({
                        text: state.error.error ? state.error.error : '',
                        type: ETextType.p,
                        props: {
                          styles: {
                            desktop: {
                              fontWeight: 500,
                              background: '#ea1d5d',
                              padding: pxToRem(5)
                            }
                          }
                        }
                      }),
                      Spacer({ height: pxToRem(30) }),
                      Form({
                        inputs: [
                          {
                            title: 'username',
                            name: 'username',
                            placeholder: 'admin',
                            type: 'text',
                            error: !!state.error.error,
                            style: { padding: 0 }
                          },
                          {
                            title: 'password',
                            name: 'password',
                            placeholder: '***********',
                            error: !!state.error.error,
                            type: 'password'
                          }
                        ],
                        children: [
                          Container({
                            children: [
                              Button({
                                text: 'Logar',
                                props: {
                                  model: state.error.error ? 'error' : 'dark',
                                  type: 'submit'
                                }
                              })
                            ]
                          })
                        ],
                        props: {
                          size: 'small',
                          onSubmit: async (e: any) => {
                            state.loading.setLoading(false)
                            e.preventDefault()
                            const values = {
                              username: e.target[0].value,
                              password: e.target[1].value
                            }
                            await controller.login(values)
                          }
                        }
                      })
                    ],
                    props: {
                      styles: {
                        desktop: {
                          justifyContent: 'center',
                          flex: 1,
                          maxWidth: '300px',
                          width: '100%',
                          alignItems: 'center'
                        }
                      }
                    }
                  })
                  : Column({
                    props: {
                      styles: {
                        desktop: {
                          height: '100%',
                          justifyContent: 'center'
                        }
                      }
                    },
                    children: [
                      Loading({
                        props: {
                          style: {
                            maxWidth: pxToRem(150)
                          }
                        }
                      })
                    ]
                  })
              ]
            })
          ]
        })
      ],
      style: { height: '100%' }
    })
  }
}
