import { IComponents } from 'infra/types/components/index'
import { NavigateFunction } from 'react-router-dom'
import { ILoginState, ILoginValue } from './login-interface'
import { LoginRequest } from './login-request'

const http = new LoginRequest()
export class LoginController implements IComponents<ILoginValue, ILoginState> {
    public state: ILoginState;
    public history: NavigateFunction;
    constructor (state: ILoginState, history: NavigateFunction) {
      this.state = state
      this.history = history
    }

    async login (props: ILoginValue): Promise<void> {
      const result = await http.login(props)
      if (result.status) {
        sessionStorage.setItem('token', result.body.token)
        sessionStorage.setItem('user', JSON.stringify(result.body))
        this.state.context.onLogin(result.body.token, result.body)
        setTimeout(() => {
          this.state.loading.setLoading(true)
        }, 3000)
        this.history('/browse')
      } else {
        setTimeout(() => {
          this.state.loading.setLoading(true)
        }, 3000)
        this.state.error.setError(result.message)
      }
    }
}
