import Http, { HttpResponse } from 'infra/http'
import { IHttpOutput } from 'infra/http/interface-http'
import { ILoginResult, ILoginValue } from './login-interface'

export class LoginRequest {
  async login ({ username, password }: ILoginValue): Promise<IHttpOutput<ILoginResult>> {
    const result = await Http.post<ILoginResult>('/authentication', { username, password })
    return HttpResponse<ILoginResult>(result)
  }
}
