import Http, { HttpResponse } from 'infra/http'
import { IHttpOutput } from 'infra/http/interface-http'
import { IBrowseResult } from '../browse/browse-interface'
import { IDetailMovieResult } from './detail-movie-interface'

export class DetailMovieRequest {
  async getData (id: string): Promise<IHttpOutput<IDetailMovieResult>> {
    const result = await Http.get<IDetailMovieResult>(`/movie/${id}`)
    return HttpResponse<IDetailMovieResult>(result)
  }

  async getList (): Promise<IHttpOutput<IBrowseResult>> {
    const result = await Http.get<IBrowseResult>('/browser')
    return HttpResponse<IBrowseResult>(result)
  }
}
