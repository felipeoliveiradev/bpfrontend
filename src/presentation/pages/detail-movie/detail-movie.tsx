import { Layout } from 'presentation/packages'
import React, { useEffect, useState } from 'react'
import { useNavigate , useParams } from 'react-router-dom'
import { DetailMovieController } from './detail-movie-controller'
import { IDetailMovieState } from './detail-movie-interface'
import { layout } from './detail-movie-layout'
export const DetailMovie: React.FC = () => {
  const { id, category } = useParams()
  const [data, setData] = useState()
  const [movies, setMovies] = useState()
  const [error, setError] = useState()
  const [loading, setLoading] = useState(true)

  const state: IDetailMovieState = {

    loading: { loading, setLoading },
    data: { data, setData },
    error: { error, setError },
    movies: { movies, setMovies }
  }
  const history = useNavigate()
  const controller = new DetailMovieController(state, history, { category, id })
  useEffect(() => {
    controller.getData(id)
    controller.getCategory()
  },[id])

  return <Layout {...layout({ state, values: { id, category } })} />
}
