import { IListMovieItem } from 'presentation/components/list-movie/list-movie-interface'

export interface IDetailMovieState {
  loading: {
    setLoading: any
    loading: any
  }
  data: {
    setData: any
    data: IDetailMovieResult
  }
  error: {
    setError: any
    error: any
  }
  movies: {
    setMovies: any
    movies: any
  }
}
export interface IDetailMovieValue {
  id: string
  category: string
}
export interface IDetailMovieResult {
  image_url: string
  title?: string
  tags?: Array<[]>
  content_type?: string
  link?: string
  description?: string
  movies?: IListMovieItem[]
}
