import { Banner } from 'presentation/components'
import {
  Button,
  Column,
  Container,
  Line,
  pxToRem,
  Spacer,
  Tags,
  Text
} from 'presentation/packages'
import { ListMovie } from 'presentation/components/list-movie/list-movie'
import { ETextType } from 'presentation/packages/ui/flupper/Text/interfaces'
import { EBannerSize } from 'presentation/components/banner/banner-interface'
import { IComponents } from 'infra/types'
import { IDetailMovieState, IDetailMovieValue } from './detail-movie-interface'
export const layout = (props: IComponents<IDetailMovieValue, IDetailMovieState>): object => {
  const { state, values } = props
  console.log(values)
  return {
    children: Container({
      children: [
        Banner({
          size: EBannerSize.LARGE,
          image: state?.data?.data?.image_url,
          loading: state.loading.loading
        }),
        Line({
          children: [
            Column({
              children: [
                Text({
                  text: state?.data?.data?.title,
                  type: ETextType.h1,
                  props: {
                    styles: {
                      desktop: {
                        marginTop: pxToRem(-30),
                        fontWeight: 600,
                        fontFamily: 'Open Sans',
                        fontSize: pxToRem(39)
                      }
                    }
                  }
                })
              ]
            }),
            Spacer({ height: pxToRem(24) }),
            Line({
              children: [
                Tags({
                  items: state?.data?.data?.tags || []
                })
              ]
            }),
            Spacer({ height: pxToRem(25) }),
            Line({
              children: [
                Button({
                  text: 'Assistir agora',
                  loading: state.loading.loading,
                  props: {
                    model: 'dark',
                    type: 'submit',
                    size: 'medium',
                    onClick: () => {
                      window.open(state.data.data.link, '_blank')
                    },
                    styles: {
                      desktop: {
                        left: 0,
                        backgroundColor: '#fff',
                        color: '#000000',
                        fontWeight: 600,
                        borderRadius: pxToRem(3)
                      }
                    }
                  }
                })
              ]
            }),
            Spacer({ height: pxToRem(47) }),
            Line({
              children: [
                Text({
                  text: state?.data?.data?.description,
                  type: ETextType.h6,
                  props: {
                    styles: {
                      desktop: {
                        lineHeight: pxToRem(30),
                        marginTop: pxToRem(-30),
                        fontWeight: 300
                      }
                    }
                  }
                })
              ]
            })
          ],
          props: {
            styles: {
              desktop: {
                width: 'calc(100% - 113px)',
                flexDirection: 'column',
                margin: 'auto'
              },
              tablet: {
                width: 'calc(100% - 10px)'
              }
            }
          }
        }),
        Line({
          children: [
            Spacer({ height: pxToRem(54) }),
            ListMovie({
              items: state?.movies?.movies,
              loading: state.loading.loading,
              category: values.category,
              title: 'Veja também'
            }),
            Spacer({ height: pxToRem(40) })
          ]
        })
      ],
      props: {
        styles: { desktop: { background: '#151515' }, tablet: { background: '#000' } }
      }
    })
  }
}
