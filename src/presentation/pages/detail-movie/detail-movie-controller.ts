import { IComponents } from 'infra/types'
import { IListMovieItem } from 'presentation/components/list-movie/list-movie-interface'
import { NavigateFunction } from 'react-router-dom'
import { IDetailMovieState, IDetailMovieValue } from './detail-movie-interface'
import { DetailMovieRequest } from './detail-movie-request'

const http = new DetailMovieRequest()
export class DetailMovieController
implements IComponents<IDetailMovieValue, IDetailMovieState> {
    public state?: IDetailMovieState;
    public history?: NavigateFunction;
    public values?: IDetailMovieValue;

    constructor (
      state?: IDetailMovieState,
      history?: NavigateFunction,
      values?: IDetailMovieValue
    ) {
      this.state = state
      this.history = history
      this.values = values
    }

    async getData (id: string): Promise<void> {
      const result = await http.getData(id)
      if (result.status) {
        await this.state?.data?.setData(result.body)
      } else {
        console.log(result.message)
        await this.state.error.setError(result.message)
      }
      setTimeout(() => {
        this.state?.loading?.setLoading(false)
      }, 2000)
    }

    async getCategory (): Promise<void> {
      this.state.loading.setLoading(true)
      try {
        const result = await http.getList()
        if (result.status) {
          const movies = result.body[this.values?.category].filter(
            (item: IListMovieItem) => item.id !== this.values.id
          )
          if (movies) {
            this.state.movies.setMovies(movies)
          }
          return
        } else {
          console.log(result.message)
          this.state.error.setError(result.message)
        }
        setTimeout(() => {
          this.state?.loading?.setLoading(false)
        }, 2000)
      } catch (err) {
        console.log(err.message)
        this.history('/browse')
        setTimeout(() => {
          this.state?.loading?.setLoading(false)
        }, 2000)
      }
    }
}
