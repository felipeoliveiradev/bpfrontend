import { IComponents } from 'infra/types'
import { ListMovie, Banner } from 'presentation/components'
import { EBannerSize } from 'presentation/components/banner/banner-interface'
import { Container, pxToRem, Spacer } from 'presentation/packages'
import { IBrowseState, IBrowseValue } from './browse-interface'
export const layout = (props: IComponents<IBrowseValue, IBrowseState>): object => {
  const { state } = props
  console.log(state.data)
  return {
    children: Container({
      children: [
        Banner({
          image: state?.data?.data?.featured_image,
          loading: state.loading.loading,
          size: EBannerSize.SMALL,
          props: {
            styles: {
              tablet: { backgroundImage: `url(${state?.data?.data?.featured_image_mobile})`, backgroundPosition: 'bottom', backgroundSize: 'cover', height: pxToRem(479) }

            }
          }
        }),
        Spacer({ props: { styles: { desktop: { height: pxToRem(60) }, mobile: { height: pxToRem(39) } } } }),
        ListMovie({
          items: state.data.data ? state.data.data.series : [],
          category: 'series',
          title: 'Séries',
          loading: state.loading.loading
        }),
        Spacer({ height: pxToRem(40) }),
        ListMovie({
          items: state.data.data ? state.data.data.training : [],
          title: 'Núcleo de formação',
          category: 'training',
          loading: state.loading.loading
        }),
        Spacer({ height: pxToRem(40) })
      ],
      props: {
        styles: { desktop: { background: '#151515' }, tablet: { background: '#000' } }
      }
    })
  }
}
