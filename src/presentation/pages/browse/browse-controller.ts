import { IComponents } from 'infra/types'
import { NavigateFunction } from 'react-router-dom'
import { IBrowseState, IBrowseValue } from './browse-interface'
import { BrowseRequest } from './browse-request'

const http = new BrowseRequest()
export class BrowseController
implements IComponents<IBrowseValue, IBrowseState> {
    public state?: IBrowseState;
    public history?: NavigateFunction;
    constructor (state?: IBrowseState, history?: NavigateFunction) {
      this.state = state
      this.history = history
    }

    async getData (): Promise<void> {
      const result = await http.getData()

      if (result.status) {
        const userInfo = JSON.parse(sessionStorage.getItem('user'))
        userInfo.image = result.body.profile_image
        sessionStorage.setItem('user', JSON.stringify(userInfo))
        await this.state?.context?.onPicture(result.body.profile_image)
        await this.state?.data?.setData(result.body)
      } else {
        console.log(result.message)
        await this.state.error.setError(result.message)
      }
      setTimeout(() => {
        this.state?.loading?.setLoading(false)
      }, 2000)
    }
}
