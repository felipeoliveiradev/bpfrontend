import { IListMovieItem } from 'presentation/components/list-movie/list-movie-interface'

export interface IBrowseState {
  loading: {
    setLoading: any
    loading: any
  }
  data: {
    setData: any
    data: IBrowseResult
  }
  error: {
    setError: any
    error: any
  }
  context: {
    onPicture: any
  }
}
export interface IBrowseValue {
}
export interface IBrowseResult {
  movies?: IListMovieItem[]
  featured_image: string
  featured_image_mobile: string
  profile_image: string
  series: IListMovieItem[]
  training: IListMovieItem[]
}
