import Http, { HttpResponse } from 'infra/http'
import { IHttpOutput } from 'infra/http/interface-http'
import { IBrowseResult } from './browse-interface'

export class BrowseRequest {
  async getData (): Promise<IHttpOutput<IBrowseResult>> {
    const result = await Http.get<IBrowseResult>('/browser/')
    return HttpResponse<IBrowseResult>(result)
  }
}
