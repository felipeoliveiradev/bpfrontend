import { useAuth } from 'main/contexts'
import { Layout } from 'presentation/packages'
import React, { useEffect, useState } from 'react'
import { BrowseController } from './browse-controller'
import { IBrowseState } from './browse-interface'
import { layout } from './browse-layout'
export const Browse = (): JSX.Element => {
  const [data, setData] = useState()
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState()
  const { onPicture } = useAuth()
  const state: IBrowseState = {
    loading: { loading, setLoading },
    data: { data, setData },
    error: { error, setError },
    context: { onPicture }
  }
  const controller = new BrowseController(state)

  useEffect(() => {
    controller.getData()
  },[])
  return (<Layout {...layout({ state, values: {} })} />)
}
