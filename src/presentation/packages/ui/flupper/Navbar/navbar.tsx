import React from "react";
import { Container, List } from "./styles";
import { INavbarProps, IDataProps } from "./interfaces";

export const Navbar: React.FC<INavbarProps> = ({ items, props }) => {
  return (
    <Container {...props}>
      <ul>
        {items.map((item:IDataProps, index: number) => (
          <List status={item.status} {...item} key={index}>
            {item.icon && item.icon.element}
            {item.link && <a href={item.link}>{item.name}</a> || <>{item.name}</>}
          </List>
        ))}
      </ul>
    </Container>
  );
};
