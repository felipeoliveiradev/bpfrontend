import { CSSProperties } from "styled-components";

export interface INavbarProps {
    onClick?: (props: IDataProps) => any;
    items: Array<IDataProps>;
    props?: IProps;
}

export interface IDataProps {
    name?: string;
    status?: boolean;
    link?: string;
    icon?: INavSvg;
    props?: IProps;
    onClick?: any;
}

export interface INavSvg {
    element?: any,
    props?:IProps
}
export interface IListStylesProps {
    status?: boolean;
}

export interface IProps {
    style?: CSSProperties;
    className?: string;
    styles?: IStyles;
    gap?: string;
}

export interface IStyles {
    mobile?: CSSProperties;
    tablet?: CSSProperties;
    desktop?: CSSProperties;
}
