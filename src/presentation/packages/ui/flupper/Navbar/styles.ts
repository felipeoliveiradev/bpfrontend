import styled, { css } from "styled-components";
import { jsToCss, pxToRem } from "../Configs";
import { IDataProps, IListStylesProps, IProps } from "./interfaces";

export const Container = styled.div`
    flex: 1;
    ul {
        display: flex;
        padding: 0;
        list-style: none;
        margin: 0;
        height: 100%;
        gap: ${(props: IProps) => (props.gap ? pxToRem(60) : "unset")};
        flex: 1;
        font-size: ${pxToRem(14)};
        line-height: ${pxToRem(14)};
        font-style: normal;
        font-weight: 400;

        ${(props: IProps) =>
            props.styles?.desktop &&
            css`
                ${jsToCss(props.styles?.desktop)}
            `};

        @media (max-width: ${(props: any) =>
                pxToRem(props.theme.query?.tablet)}) {
            max-width: 100%;
            flex: 0 auto;
            ${(props: IProps) =>
                props.styles?.tablet &&
                css`
                    ${jsToCss(props.styles?.tablet)}
                `};
        }
        @media (max-width: ${(props: any) =>
                pxToRem(props.theme.query?.cellphone)}) {
            ${(props: IProps) =>
                props.styles?.mobile &&
                css`
                    ${jsToCss(props.styles?.mobile)}
                `};
        }
    }
`;

export const List = styled.li<IDataProps>`
    color: #fff;
    transition: all 0.3s linear;
    cursor: default;
    user-select: none;
    align-items: center;
    justify-content: center;
    display: flex;
    cursor: pointer;

    a {
        color: #fff;
        text-decoration: none;
        cursor: pointer;
    }

    &:hover {
        color: #fff;
        transition: all 0.3s linear;
    }

    ${(props) =>
        props.status &&
        css`
            color: #fff;
            transition: all 0.3s linear;
        `}

    ${(prop: IDataProps) =>
        prop.props?.styles?.desktop &&
        css`
            ${jsToCss(prop.props?.style?.desktop)}
        `};

    @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
        max-width: 100%;
        flex: 0 auto;
        ${(prop: IDataProps) =>
            prop.props?.styles?.tablet &&
            css`
                ${jsToCss(prop.props?.styles?.tablet)}
            `};
    }
    @media (max-width: ${(props: any) =>
            pxToRem(props.theme.query?.cellphone)}) {
        ${(prop: IDataProps) =>
            prop.props?.styles?.mobile &&
            css`
                ${jsToCss(prop.props?.styles?.mobile)}
            `};
    }
`;
