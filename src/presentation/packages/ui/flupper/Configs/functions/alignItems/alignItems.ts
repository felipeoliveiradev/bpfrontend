export const alignItems = (align?: string) => {
  switch (align) {
    case "all":
      return "center";
    case "vertical":
      return "center";
    default:
      return;
  }
}