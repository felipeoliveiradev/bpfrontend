import { pxToRem } from "../../../helpers/pxToRem/pxToRem";

export const maxWidthButton = (size?: string | number) => {
  switch (size) {
    case "small":
      return pxToRem(100);
    case "medium":
      return pxToRem(230);
    case "large":
      return pxToRem(500);
    case "full":
      return;
    default:
      return "rgb(255, 255, 255)"
  }
}