import { pxToRem } from "../../../helpers/pxToRem/pxToRem";

export const maxHeightButton = (size?: string | number) => {
  switch (size) {
    case "small":
      return pxToRem(100);
    case "medium":
      return pxToRem(300);
    case "large":
      return pxToRem(500);
    case "full":
      return;
    default:
      return "rgb(255, 255, 255)"
  }
}