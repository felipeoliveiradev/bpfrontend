import { pxToRem } from "../Configs";

export const Grid = () => `
      max-width: ${pxToRem(960)};
      margin-left: auto;
      margin-right: auto;
`;
