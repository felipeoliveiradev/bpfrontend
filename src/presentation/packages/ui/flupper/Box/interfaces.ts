import { CSSProperties } from "styled-components";

// tslint:disable-next-line: no-empty-interface
export interface IBox {
  child?: any;
  props?: IProps;
 
}
export interface IProps {
  style?: CSSProperties;
  styles?: IStyles
  centered?:  ECentered;
  size?: string;
}
export enum ECentered {
  disabled = "none",
  all = "all",
  vertical = "vertical",
  horizontal = "horizontal"
  
}
export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}