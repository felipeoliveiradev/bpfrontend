import { alignItems, jsToCss, maxWidthArea, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const Container = styled.div<IProps>`
    display: flex;
    background: rgb(47, 50, 71);
    width: -webkit-fill-available;
    flex: 0 auto;
     flex-wrap: wrap;
    padding: 1.875rem 2.125rem;
    min-width: 18.75rem;
    margin-right: 15px;
    margin-bottom: 1rem;
    max-width: ${(props: IProps) => maxWidthArea(props.size)};
    justify-content: ${(props: IProps) => alignItems(props.centered)};
    align-items: ${(props: IProps) => alignItems(props.centered)};

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 100%;
    flex: 0 auto;
    ${(props: IProps) =>
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    margin-right: 0;
    width: 100%;
    padding: 0;
    ${(props: IProps) =>
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
