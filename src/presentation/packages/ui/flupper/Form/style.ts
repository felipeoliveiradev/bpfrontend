import { alignItems, jsToCss, maxWidthArea, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";
import { fade } from "../styles/themes/light/keyframe";

export const Container = styled.form<IProps>`
  width: -webkit-fill-available;
  height: auto;
  max-width: ${(props: IProps) => maxWidthArea(props.size)};
  justify-content: ${(props: IProps) => alignItems(props.centered)};
  align-items: ${(props: IProps) => alignItems(props.centered)};
  animation: ${fade} 0.2s linear forwards;
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

  @media (max-width: ${(props) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 90%;
    > div{
      display: grid;
      grid-template-columns: 1fr !important;
    }
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }

  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;

