import { CSSProperties } from "styled-components";
import { IInput } from "../Input/interfaces";

export interface IForm {
  inputs: Array<IInput>;
  children?: any;
  props?:IProps
}

export interface IProps {
  style?: CSSProperties;
  className?: any;
  onSubmit?: any,
  onChange?: any
  size?: string | number;
  centered?: string;
  styles?: IStyles;
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}