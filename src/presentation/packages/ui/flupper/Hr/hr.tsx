import { Container } from "./style";
import { ISeparator } from "./interfaces";
import React from "react";

export const Hr = (prop: ISeparator) => {
  const { props } = prop;
  return <Container {...props} />;
};
