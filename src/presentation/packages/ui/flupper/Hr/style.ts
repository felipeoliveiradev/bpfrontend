import { jsToCss, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const Container = styled.hr<IProps>`
  width: -webkit-fill-available;
  height: ${pxToRem(10)};
  border: none;
  max-width: 100%;
  background: #00cd69;
  display: flex;
  justify-content: flex-start;
  margin-left: 0;

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {

    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
