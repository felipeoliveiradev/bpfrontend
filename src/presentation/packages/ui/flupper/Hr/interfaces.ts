import { CSSProperties } from "styled-components";

// tslint:disable-next-line: no-empty-interface
export interface ISeparator {
  props?: IProps
}

export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}