import { Container } from "./style";
import { IColumn } from "./interfaces";
import { IType } from "../interfaces";
import React from "react";

export const Column = (prop: IColumn & IType) => {
  const { children: child, props} = prop;
  return (
    <Container {...props}>
      {child}
    </Container>
  );
};
