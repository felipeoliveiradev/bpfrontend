import { alignItems, jsToCss, maxWidthArea, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const Container = styled.div<IProps>`
    flex: 1;
    display: flex;
    flex-direction: column;
    position: relative;
    transition: all linear 0.2s; 
    width: auto;
    max-width: ${(props: IProps) => maxWidthArea(props.size)};
    justify-content: ${(props: IProps) => alignItems(props.centered)};
    align-items: ${(props: IProps) => alignItems(props.centered)};

    ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 100%;
    flex: 0 auto;
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
    
  }
`;
