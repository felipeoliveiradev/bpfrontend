import { alignItems, jsToCss, maxHeightArea, maxWidthArea, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const Container = styled.div<IProps>`
  display: flex;
  flex: ${(props: IProps) => props.flex === undefined ? "0 auto" : `${props.flex}`};
  flex-wrap: wrap;
  flex-direction: "";
  width:  100%;
  max-height: ${(props: IProps) => maxHeightArea(props.sizeHeight)};
  max-width: ${(props: IProps) => maxWidthArea(props.sizeWidth)};
  justify-content: ${(props: IProps) => alignItems(props.centered)};
  align-items: ${(props: IProps) => alignItems(props.centered)};


  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 100%;
    flex: 0 auto;
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};    
  }
`;
