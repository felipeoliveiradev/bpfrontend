import { Container } from "./style";
import { ILine } from "./interfaces";
import { IType } from "../interfaces";
import React from "react";

export const Line = (prop: ILine & IType) => {
  const { children: child, props } = prop;
  return (
    <Container {...props}>
      {child}
    </Container>
  );
};
