import { CSSProperties } from "styled-components";

export interface ILine {
  style?: any
  children: any
  props?: IProps
}

export interface IProps {
  style?: CSSProperties; 
  sizeHeight?: string;
  sizeWidth?: string;
  size?: string;
  centered?: string;
  className?: string;
  backgroundColor?: string;
  flex?: string |  number;
  styles?: IStyles;
}


export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}
