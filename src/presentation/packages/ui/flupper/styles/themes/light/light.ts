import { pxToRem } from "presentation/packages";
import { DefaultTheme } from "styled-components";

export const light = {
  title: "light",
  typography: {
    fontFamily: "Nunito",
    h1: {
        size: pxToRem(40)
    },
    h2: {
        size: pxToRem(32)
    },
    h3: {
        size: pxToRem(28)
    },
    h4: {
        size: pxToRem(24)
    },
    h5: {
        size: pxToRem(20)
    },
    h6: {
        size: pxToRem(16)
    },
    p: {
        size: pxToRem(10)
    },
    span: {
        size: pxToRem(10)
    }
  },
  colors: {
    background: "#000",
    backgroundColor: "#ffffff",
    footer: "",
    footerText: "",
    header: "",
    headerText: "",
    overlay: "",
    primary: "#000000",
    secundary: "#333",
    third: "#0090ff",
    fourth: "#caccd1",
    text: "#fff",
    border: "#000",
    boxShadow: "#000",
    black: '#000',
    error: '#fd5c63'
  },
  measures: {
    borderRadius: 3,
  },
  query: {
    cellphone: 500,
    tablet: 800,
    desktop: 1280,
  },

  animations: {
    transition: "0.6s",
    afterTransition: '1.2s'
  },
};
