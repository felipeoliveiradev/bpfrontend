import "styled-components";
declare module "styled-components" {
    export interface DefaultTheme {
        title: string;
        typography: {
            fontFamily: string;
            h1: {
                size: string;
            };
            h2: {
                size: string;
            };
            h3: {
                size: string;
            };
            h4: {
                size: string;
            };
            h5: {
                size: string;
            };
            h6: {
                size: string;
            };
            p: {
                size: string;
            };
            span: {
                size: string;
            };
        };
        colors: {
            background: string;
            backgroundColor: string;
            footer: string;
            footerText: string;
            header: string;
            headerText: string;
            overlay: string;
            primary: string;
            secundary: string;
            third: string;
            fourth: string;
            text: string;
            border: string;
            boxShadow: string;
            black: string;
            error: string;
        };
        measures: {
            borderRadius: number;
        };
        query: {
            cellphone: number;
            tablet: number;
            desktop: number;
        };
        animations: {
            transition: string;
            afterTransition: string;
        };
    }
}
