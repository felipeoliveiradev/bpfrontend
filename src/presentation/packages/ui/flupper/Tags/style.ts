import { jsToCss, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";
import { fade } from "../styles/themes/light/keyframe";

export const Container = styled.ul<IProps>`
  list-style: none;
  animation: ${fade} 0.2s linear forwards;
  display: flex;
  justify-content: flex-start;
  width: -webkit-fill-available;
  padding: 0;
  gap: 10px;
  li {
      transition: all 0.3s linear 0s;
      background: transparent;
      padding: 2px 10px;
      border: 1px solid #E1E4EF;
      font-weight: 600;
      box-sizing: border-box;
      border-radius: 2px;
      color: rgb(255, 255, 255);
      font-style: normal;
        font-weight: 300;
        font-size: 12px;
        line-height: 16px;
  }

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

@media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {

  ${(props: IProps) => 
    props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  
}
@media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
  ${(props: IProps) => 
    props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
}
`;
