import { IItem, ITags } from "./interfaces";

import { Container } from "./style";
import React from "react";

export const Tags = (prop: ITags) => {
  const { items, props } = prop;
  return (
    <Container {...props}>
      {items.map((item: IItem, index: number) => {
          if(item.title){
            return (
                <>
                  <li key={index}>{item.title}{item.number ? `<span>${item.number}</span>`: ""}</li>
                </>
              );
          } else {
              return(<li key={index}>{item}</li>)
          }
       
      })}
    </Container>
  );
};
