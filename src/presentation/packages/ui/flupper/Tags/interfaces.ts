import { CSSProperties } from "styled-components";

// tslint:disable-next-line: no-empty-interface
export interface ITags {
  items: Array<IItem | []>;
  props?: IProps;
}

export interface IListStyles {
  titleColor?: string;
  subtitleColor?: string;
  padding?: boolean;
}

export interface IItem {
  title: string;
  number?: string;
  icon?: any;
  style?: CSSProperties;
}


export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}