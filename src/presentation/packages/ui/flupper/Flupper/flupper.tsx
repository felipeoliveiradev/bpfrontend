import { ContainerAll } from "./style";
import { IContainer } from "./interfaces";
import { IType } from "../interfaces";
import React from "react";

export const Flupper = (prop: IContainer & IType) => {
  const { child, style, props} = prop;
  return (
    <ContainerAll style={style} {...props}>
      {child}
    </ContainerAll>
  );
};
