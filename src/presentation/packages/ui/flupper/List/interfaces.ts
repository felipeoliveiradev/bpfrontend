import { CSSProperties } from "styled-components";

export interface IList {
  items: Array<IItem>;
  props?: IProps
}

export interface IItem {
  title: string;
  link?: string;
  subtitle?: string;
  icon?: any;
}

export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles
  titleColor?: string;
  subtitleColor?: string;
  padding?: boolean;
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}