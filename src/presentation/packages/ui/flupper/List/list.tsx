import { IItem, IList } from "./interfaces";

import { Container } from "./style";
import React from "react";

export const List = (prop: IList) => {
    const { items, props } = prop;
    return (
        <Container {...props}>
            {items.map((item: IItem, index: number) => {
                return (
                    <>
                        <li>
                            {(item.link && (
                                <a key={index}>
                                    <div>{item?.icon}</div>
                                    <div>
                                        <ul>
                                            <li>{item?.title}</li>
                                            <li>{item?.subtitle}</li>
                                        </ul>
                                    </div>
                                </a>
                            )) || (
                                <>
                                    <div>{item?.icon}</div>
                                    <div>
                                        <ul>
                                            <li>{item?.title}</li>
                                            <li>{item?.subtitle}</li>
                                        </ul>
                                    </div>
                                </>
                            )}
                        </li>
                        <br />
                    </>
                );
            })}
        </Container>
    );
};
