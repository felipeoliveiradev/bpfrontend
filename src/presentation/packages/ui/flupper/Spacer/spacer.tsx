import { Container } from "./style";
import { ISpacer } from "./interfaces";
import React from "react";

export const Spacer = (prop: ISpacer) => {
  const {width, height, props} = prop;
  return (
    <Container width={width} height={height} {...props} />
  );
};
