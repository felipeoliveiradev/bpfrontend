import { IProps, ISpacer } from "./interfaces";
import { jsToCss, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

export const Container = styled.div<ISpacer> `
  display: flex;
  width: ${(props: any) => props?.width ? props.width : pxToRem(15)};
  height: ${(props: any) => props?.height ? props.height : pxToRem(15)};

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 100%;
    flex: 0 auto;
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
