import { Container } from "./styles";
import { IButton } from "./interface";
import React from "react";
import Skeleton from "react-loading-skeleton";

export const Button = (prop: IButton) => {
    const { text, props, loading } = prop;
    return (
        (loading && loading === true && <Skeleton count={2} />) || (
            <Container {...props}>{text}</Container>
        )
    );
};
