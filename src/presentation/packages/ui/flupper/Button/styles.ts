import { jsToCss, maxWidthButton, pxToRem } from '../Configs';
import styled, { css } from 'styled-components';

import { IProps } from './interface';

export const Container = styled.button<IProps>`

    background-color: ${(props: IProps) => {
      switch (props.model) {
        case "dark":
          return "#2b2e43"
        case "error":
          return "#ea1d5d"
        default:
          return "rgb(2, 197, 103)"
      }
    }};
    color: ${(props: IProps) => {
      switch (props.model) {
        case "dark":
          return "#fff"
          case "error":
          return "#fff"
        default:
          return "rgb(255, 255, 255)"
      }
    }};
    font-weight: 300;
    display: flex;
    justify-content: center;
    border-radius: 5px;
    padding: 10px;
    margin-bottom: 10px;
    cursor: pointer;
    flex: 1;
    max-width: ${(props: IProps) => maxWidthButton(props.size)};
    align-items: center;
    outline: none;
    box-shadow: none;
    border: none;   

    ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

    @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
      ${(props: IProps) => 
        props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    }
    @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
      ${(props: IProps) => 
        props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
    }
`;
