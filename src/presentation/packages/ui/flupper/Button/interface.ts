import { CSSProperties } from "styled-components";

export interface IButton{
  props?: IProps;
  text: string;
  type?: string;
  loading?: boolean;
}

export interface IProps {
  style?: CSSProperties;
  type?: any;
  model?: string;
  onChange?: any;
  onClick?: any;
  ref?: any
  size?: any;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}