import { CSSProperties } from "styled-components";

export interface IText {
  type: ETextType,
  text: string,
  props?: IProps,
  loading?: boolean
}
export enum ETextType {
  h1 = "h1",
  h2 = "h2",
  h3 = "h3",
  h4 = "h4",
  h5 = "h5",
  h6 = "h6",
  p = "p",
  span = "span"
}
export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}