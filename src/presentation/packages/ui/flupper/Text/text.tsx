import { ETextType, IText } from "./interfaces";
import { H1, H2, H3, H4, H5, H6, P, Span } from "./style";

import { IProps } from "../Column/interfaces";
import React from "react";
import Skeleton from "react-loading-skeleton";

export const Text = (prop: IText) => {
  const { type, text, props, loading = true } = prop;

  return (
    <>
      {loading ? text && textTratament(type, text, props) : <Skeleton />}
    </>
  );
};


const textTratament = (type: string, text: string, props?: IProps) => {
  switch (type) {
    case ETextType.h1:
      return <H1 {...props}>{text}</H1>;
    case ETextType.h2:
      return <H2 {...props}>{text}</H2>;
    case ETextType.h3:
      return <H3 {...props}>{text}</H3>;
    case ETextType.h4:
      return <H4 {...props}>{text}</H4>;
    case ETextType.h5:
      return <H5 {...props}>{text}</H5>;
    case ETextType.h6:
      return <H6 {...props}>{text}</H6>;
    case ETextType.p:
      return <P {...props}>{text}</P>;
    case ETextType.span:
      return <Span {...props}>{text}</Span>
    default:
      return;
  }
}