import { jsToCss, pxToRem } from "..";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";
import { fade } from "../styles/themes/light/keyframe";

export const H1 = styled.h1<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  z-index: 2;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h1.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const H2 = styled.h2<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h2.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const H3 = styled.h3<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h3.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const H4 = styled.h4<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h4.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const H5 = styled.h5<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h5.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const H6 = styled.h6<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color:${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.h6.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const P = styled.p<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.p.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
export const Span = styled.span<IProps>`
 animation: ${fade} 0.2s linear forwards;
 transition: all linear 0.2s; 
  padding: 0;
  margin: 0;
  line-height: 1;
  color: ${(props) => props.theme.colors.text};
  font-size: ${(props) => props.theme.typography.span.size};
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;

