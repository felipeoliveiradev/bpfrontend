import { InputHTMLAttributes } from "react";
import { CSSProperties } from "styled-components";

export interface IInput extends InputHTMLAttributes<HTMLInputElement> {
  title: string;
  error?: boolean;
  message?: string;
  required?: boolean;
  type?: string;
  props?: IProps
  background?: string;
  flex?: string;
}

export interface IBypass {
  title: string;
  index: number;
}

export interface IInputStyles {
  error?: boolean;
  message?: string;
  styles?: IStyles;
  background?: string;
}

export interface IErrorData {
  name: string;
  state: boolean;
  bypass?: IBypass;
}

export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles;
  inputs?: IInputStyles;
  flex?: string;
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}