import { Container, InputGroup } from "./style";
import React, { forwardRef } from "react";

import { IInput } from "./interfaces";

export const Input: React.RefForwardingComponent<HTMLInputElement, IInput> = (
  { title, error, message,type, flex, background, props, ...rest },
  ref
) => {
  return (
    <Container flex={flex}>
      <InputGroup {...props} background={background} error={error}>
        <span>{title}</span>
        <input ref={ref}  {...rest} type={type} />
        {error && message && <span>{message}</span>}
      </InputGroup>
    </Container>
  );
};

export default forwardRef(Input);
