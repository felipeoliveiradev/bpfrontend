import { IInputStyles, IProps } from "./interfaces";
import { jsToCss, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { fade } from "../styles/themes/light/keyframe";

export const Container = styled.div<IProps>`
  width: 100%;
  flex: ${props => props?.flex && props.flex};

  ${(props: IProps) =>
    props.styles?.desktop &&
    css`
      ${jsToCss(props.styles?.desktop)}
    `};

  @media (max-width: ${(props: any) => pxToRem(props.theme.query.tablet)}) {
    ${(props: IProps) =>
      props.styles?.tablet &&
      css`
        ${jsToCss(props.styles?.tablet)}
      `};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query.cellphone)}) {
    ${(props: IProps) =>
      props.styles?.mobile &&
      css`
        ${jsToCss(props.styles?.mobile)}
      `};
  }
`;
export const InputGroup = styled.label<IInputStyles>`
  display: flex;
  flex-direction: column;
  background-color: ${(props) =>
    props.background ? props.background : props.theme.colors.secundary};
  padding: ${pxToRem(10)} ${pxToRem(10)} ${pxToRem(10)} ${pxToRem(10)};
  border-radius: ${pxToRem(3)};
  margin-bottom: ${pxToRem(10)};

  span {
    font-size: ${pxToRem(10)};
    font-weight: 600;
    transition: all ease 0.2s;
    color: ${(props) => (props.error ? props.theme.colors.error : props.theme.colors.text)};
    text-transform: uppercase;
    margin-bottom: ${pxToRem(5)};
    ${(props) =>
      props.error &&
      css`
        &:last-child {
          animation: ${fade} 0.2s linear;
          color:${(props) => props.theme.colors.error};
          margin-top: 4px;
        }
      `}
  }

  input {
    background: transparent;
    border: none;
    height: ${pxToRem(20)};
    color: ${(props) => (props.error ? props.theme.colors.error : props.theme.colors.text)} !important;
    outline: none;
    transition: all ease 0.2s;
  }

  input:-webkit-autofill,
  textarea:-webkit-autofill,
  select:-webkit-autofill {
    -webkit-text-fill-color: ${(props) =>
      props.error ? props.theme.colors.error : props.theme.colors.text} !important;
    -webkit-box-shadow: 0 0 0px 1000px #2f3247 inset;
    border-bottom: ${pxToRem(1)} solid
      ${(props) => (props.error ? props.theme.colors.error : props.theme.colors.text)} !important;
  }

  ${(props) =>
    props.styles?.desktop &&
    css`
      ${jsToCss(props.styles?.desktop)}
    `};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query.tablet)}) {
    ${(props) =>
      props.styles?.tablet &&
      css`
        ${jsToCss(props.styles?.tablet)}
      `};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query.cellphone)}) {
    ${(props) =>
      props.styles?.mobile &&
      css`
        ${jsToCss(props.styles?.mobile)}
      `};
  }
`;
