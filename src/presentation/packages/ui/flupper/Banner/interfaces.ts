import { CSSProperties } from "styled-components";

export type IStatus = "success" | "error" | "warning";

export interface IBannerProps {
  status: IStatus;
  title: string;
  subtitle: string;
  separator?: boolean;
  loading?: boolean;
  padding?: boolean;
  props?: IProps;
}

export interface IProps {
  style?: CSSProperties; 
  size?: string;
  centered?: string;
  className?: string;
  backgroundColor?: string;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}