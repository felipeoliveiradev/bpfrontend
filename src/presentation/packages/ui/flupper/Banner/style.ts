import {
  AiOutlineCheckCircle,
  AiOutlineCloseCircle,
  AiOutlineWarning,
} from "react-icons/ai";
import { jsToCss, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";
import { fade } from "../styles/themes/light/keyframe";

const defaultStyleIcon = css`
  font-size: ${pxToRem(24)};
  margin-right: ${pxToRem(10)};
`;

export const MuiAiOutlineCheckCircle = styled(AiOutlineCheckCircle)`
  ${defaultStyleIcon};
  color: #00cd69;
`;

export const MuiAiOutlineWarning = styled(AiOutlineWarning)`
  ${defaultStyleIcon};
  color: #fbb034;
`;

export const MuiAiOutlineCloseCircle = styled(AiOutlineCloseCircle)`
  ${defaultStyleIcon};
  color: #fd5c63;
`;

export const Container = styled.div<IProps>`
  padding-top: ${pxToRem(0)};
  margin-right: ${pxToRem(10)};
  background: rgb(47,50,71);
  flex: 1;
  margin-bottom: ${pxToRem(15)};
  border-radius: ${pxToRem(2)};
  color: #fff;
  animation: ${fade} 0.2s linear forwards;
  ul {
    display: flex;
    list-style: none;
    justify-content: flex-start;
    align-items: center;
    flex: 1;
    padding: 0 ${pxToRem(28)} 0 ${pxToRem(28)};

    li {
      padding: ${pxToRem(30)} ${pxToRem(10)} ${pxToRem(30)} ${pxToRem(10)};
      h3,
      h4 {
        padding: 0;
        margin: 0;
        line-height: 1.2;
      }
      h3 {
        margin-bottom: ${pxToRem(5)};
        font-weight: 500;
        font-size: ${pxToRem(16)};
      }
      h4 {
        font-weight: 400;
        font-size: ${pxToRem(12)};
        color: #757684;
      }
    }
  }

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};

@media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
  max-width: 100%;
  flex: 0 auto;
  ${(props: IProps) => 
    props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  
}
@media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
  ${(props: IProps) => 
    props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  
}
`;
