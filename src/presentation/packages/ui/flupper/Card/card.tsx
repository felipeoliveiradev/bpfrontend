import { Container } from "./style";
import { ICard } from "./interfaces";
import React from "react";

export const Card = (prop: ICard) => {
  const { props, text, number, subtitle, color} = prop;
  return (
    <Container {...props} color={color}>
      <header>
          <ul>
            <li></li>
            <li>{text}</li>
            <li>
            </li>
          </ul>
      </header>
      <main>
        <h3>{number}</h3>
      </main>
      <footer>
      <span>{subtitle}</span>

      </footer>
    </Container>
  );
};
