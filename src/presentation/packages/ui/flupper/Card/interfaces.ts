import { CSSProperties } from "styled-components";

export interface ICard {
    color?: string;
    icon?: any;
    number?: string;
    subtitle?: string;
    text?: string;
    props?: IProps;
}

export interface IProps {
    style?: any; 
    size?: string;
    centered?: string;
    className?: string;
    backgroundColor?: string;
    styles?: IStyles
  }
  
  export interface IStyles{
    mobile?: CSSProperties,
    tablet?: CSSProperties,
    desktop?: CSSProperties
  }