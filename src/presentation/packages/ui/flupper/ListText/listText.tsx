import { Layout } from "../Layout/layout";
import React from "react";
import { layout } from "./layout";
import { IListTextProps } from "./interfaces";

export const ListText: React.FC<IListTextProps> = ({ data }) => {
  return <Layout {...layout({ data })} />;
};
