import { Container } from "../Container/container";
import { ETextType } from "../Text/interfaces";
import { Spacer } from "../Spacer/spacer";
import { Text } from "../Text/text";
import { IListTextProps } from "./interfaces";
import { pxToRem } from "../Configs";

export const layout = (props: IListTextProps) => {
  const { data } = props;

  return {
    children: Container({
      children: [
        data.map((item) => {
          return [
            Text({
              text: `${item.title}: `,
              type: ETextType.h3,
              props: { style: { fontWeight: 600, fontSize: pxToRem(15) } },
            }),
            Spacer({ height: "0.5rem" }),
            Text({
              text: `${item.subtitle}`,
              type: ETextType.h3,
              props: {
                style: {
                  fontWeight: 300,
                  fontSize: pxToRem(13),
                  color: "#8c8e99",
                },
              },
            }),
            Spacer({ height: "1rem" }),
          ];
        }),
      ],
    }),
  };
};
