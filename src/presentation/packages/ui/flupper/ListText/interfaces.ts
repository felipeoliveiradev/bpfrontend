export interface IListTextProps {
  data: Array<IDataProps>;
}

export interface IDataProps {
  title: string;
  subtitle: string;
}
