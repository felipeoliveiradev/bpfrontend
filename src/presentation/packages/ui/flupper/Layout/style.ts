import { jsToCss, pxToRem } from "..";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const Container = styled.div<IProps>`
  flex: 1;
  display: flex;
  ${(props) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};  
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
  }
`;
