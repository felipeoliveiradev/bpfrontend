import { CSSProperties } from "styled-components";

export interface IColumn {
  children?: any;
  props?: IProps
}
export interface IProps {
  style?: CSSProperties; 
  className?: string;
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}