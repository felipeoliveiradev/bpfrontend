import { Container } from "./style";
import React from "react";

export const Layout: React.FC = (prop: any) => {
  const { children, props} = prop
  return (
    <Container {...props}>
      {children}
    </Container>
  );
};
