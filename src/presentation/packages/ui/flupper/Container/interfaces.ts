import { CSSProperties } from "styled-components";

export interface IContainer {
  style?: any;
  children?: any;
  props?: IProps;
}

export interface IProps {
  style?: CSSProperties; 
  size?: string;
  centered?: string;
  className?: string;
  backgroundColor?: string;
  styles?: IStyles;
}

export interface IStyles{
  mobile?: CSSProperties,
  tablet?: CSSProperties,
  desktop?: CSSProperties
}