import { alignItems, jsToCss, maxWidthArea, pxToRem } from "../Configs";
import styled, { css } from "styled-components";

import { IProps } from "./interfaces";

export const ContainerAll = styled.div<IProps>`
  display: flex;
  flex: 1;
  flex-direction: column;
  position: relative;
  width: auto;
  max-width: auto;
  background: "#2b2e43";
  min-width: auto;
  max-width: ${(props: IProps) => maxWidthArea(props.size)};
  justify-content: ${(props: IProps) => alignItems(props.centered)};
  align-items: ${(props: IProps) => alignItems(props.centered)};  
  
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  
  @media (max-width: ${(props) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 100%;
    ${(props: IProps) => 
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
    }
    
  @media (max-width: ${(props) => pxToRem(props.theme.query?.cellphone)}) {
    max-width: 100%;
    ${(props: IProps) => 
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};
    }
`;
