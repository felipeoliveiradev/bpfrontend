import { ContainerAll } from "./style";
import { IContainer } from "./interfaces";
import { IType } from "../interfaces";
import React from "react";

export const Container = (prop: IContainer & IType) => {
  const { children: child, props} = prop;
  return (
    <ContainerAll {...props}>
      {child}
    </ContainerAll>
  );
};
