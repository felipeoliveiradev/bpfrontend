import { CSSProperties } from 'styled-components'
export enum EBannerSize {
  SMALL = 'small',
  LARGE = 'medium'
}

export interface IBanner {
  props?: IProps
  title?: string
  image?: string
  size?: EBannerSize
  loading: boolean
}

export interface IProps {
  style?: CSSProperties
  sizeHeight?: string
  sizeWidth?: string
  size?: string
  centered?: string
  className?: string
  backgroundColor?: string
  flex?: string | number
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties
  tablet?: CSSProperties
  desktop?: CSSProperties
}
