import { pxToRem } from 'presentation/packages'
import * as React from 'react'
import Skeleton, { SkeletonTheme } from 'react-loading-skeleton'
import { EBannerSize, IBanner } from './banner-interface'
import { Container } from './banner-style'

export const Banner = (prop: IBanner): JSX.Element => {
  const { props, image, size, loading } = prop
  return (
    !loading ? <Container {...props} image={image} size={size}/> : <Skeleton style={{ height: pxToRem((size && size === EBannerSize.SMALL) ? 441 : 556) }} />
  )
}
