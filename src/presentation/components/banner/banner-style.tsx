import { jsToCss, pxToRem } from 'presentation/packages'
import styled, { css } from 'styled-components'
import { EBannerSize, IBanner, IProps } from './banner-interface'

export const Container = styled.div`
    width: 100%;
    height: ${(props: IBanner) => {
        if (props.size) {
            switch (props.size) {
                case EBannerSize.LARGE:
                    return pxToRem(556)
                default:
                    return pxToRem(441)
            }
        } else {
            return pxToRem(441)
        }
    }};
    margin-bottom: ${(props: IBanner) => {
        if (props.size) {
            switch (props.size) {
                case EBannerSize.LARGE:
                    return pxToRem(-80)
                default:
                    return 'unset'
            }
        } else {
            return 'unset'
        }
    }};
    background: ${(props: IBanner) => {
        if (props.image) {
            switch (props.size) {
                case EBannerSize.SMALL:
                    return `url(${props.image}) center center / cover;`
                default:
                    return `linear-gradient(to left, rgba(21, 21, 21, 0) 0%, rgb(21, 21, 21)), linear-gradient(rgba(21, 21, 21, 0) 80%, rgb(21, 21, 21)), url(${props.image}) center center / cover`
            }
        } else {
            return '#000'
        }
    }};
    
  ${(props: IProps) =>
      props.styles?.desktop &&
      css`
          ${jsToCss(props.styles?.desktop)}
      `};
  
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 95%;
    flex: 0 auto;
    margin-left: 0 !important;
    padding: 8px;
    margin-left: 0 !important;
    background: ${(props: IBanner) => props?.image ? `linear-gradient(180deg, rgba(2,0,36,0.5172443977591037) 52%, rgba(1,0,23,0.7441351540616247) 63%, rgba(0,0,0,1) 76%, rgba(0,0,0,1) 100%), url(${props.image}) center center / cover;` : ''}

    ${(props: IProps) =>
        props.styles?.tablet &&
        css`
            ${jsToCss(props.styles?.tablet)}
        `};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {

    ${(props: IProps) =>
        props.styles?.mobile &&
        css`
            ${jsToCss(props.styles?.mobile)}
        `};    
  }
`

export const CardMovieContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 251px !important;
    transition: all 1s;
    h6 {
        margin-left: 9px;
        margin-right: 9px;
        font-weight: 600;
    }
    p {
        margin-left: 9px;
        margin-right: 9px;
        color: #b4b8ce;
    }

    &:hover {
        transform: scale(0.9);
        transition: all 1s;
    }
`
