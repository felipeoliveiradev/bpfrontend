import { useAuth } from 'main/contexts'
import { pxToRem, Spacer } from 'presentation/packages'
import { Browse, DetailMovie, Login } from 'presentation/pages'
import React from 'react'
import { Routes, Route, Navigate } from 'react-router-dom'
import { Header } from '../header/header'
export const Router: React.FC = () => {
  return (
        <>
            <Routes>
                <Route index element={<Login />} />
                <Route
                    path="browse"
                    element={
                        <ProtectedRoute>
                            <Header mobile={false} />
                            <Browse />
                            <Spacer
                                props={{
                                  styles: { tablet: { height: pxToRem(70) } }
                                }}
                            />
                            <Header mobile={true} />
                        </ProtectedRoute>
                    }
                />
                <Route
                    path="detail-movie/:category/:id"
                    element={
                        <ProtectedRoute>
                            <Header mobile={false} />
                            <DetailMovie />
                            <Spacer
                                props={{
                                  styles: { tablet: { height: pxToRem(70) } }
                                }}
                            />
                            <Header mobile={true} />
                        </ProtectedRoute>
                    }
                />
            </Routes>
        </>
  )
}

export const ProtectedRoute = ({ children }: any): JSX.Element => {
  const { token } = useAuth()

  if (!token) {
    return <Navigate to="/" replace />
  }

  return children
}
