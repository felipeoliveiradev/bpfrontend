import { MouseEventHandler } from 'react'
import { CSSProperties } from 'styled-components'

export interface IListMovie {
  onClick?: (prop: any) => any
  props?: IProps
  items: IListMovieItem[]
  title?: string
  category: string
  loading?: boolean
}

export interface IListMovieItem {
  image_url: string
  title: string
  launch_date: string
  episodes_counter: string
  views?: string
  id: string
  category: string
  link?: string
  loading?: boolean
  teacher?: string
}

export interface IProps {
  style?: CSSProperties
  sizeHeight?: string
  sizeWidth?: string
  size?: string
  centered?: string
  className?: string
  backgroundColor?: string
  flex?: string | number
  styles?: IStyles
}

export interface IStyles{
  mobile?: CSSProperties
  tablet?: CSSProperties
  desktop?: CSSProperties
}
