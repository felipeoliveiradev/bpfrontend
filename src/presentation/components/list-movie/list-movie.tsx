import * as React from 'react'
import { Container, CardMovieContainer } from './list-movie-style'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { IListMovie, IListMovieItem } from './list-movie-interface'
import { pxToRem, Spacer, Text } from 'presentation/packages'
import { ETextType } from 'presentation/packages/ui/flupper/Text/interfaces'
import { useNavigate } from 'react-router-dom'
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css'

export const ListMovie = (prop: IListMovie): JSX.Element => {
  const { items, props, title, loading, category } = prop
  const settings = {
    dots: true,
    infinite: true,
    swipeToSlide: true,
    arrows: false,
    speed: 500,
    slidesToShow: 5,
    touchThreshold: 3,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  }
  return (
        <Container {...props}>
            {!loading
              ? (
                  title && <h4>{title}</h4>
                )
              : (
                <>
                    <Skeleton />
                    <Spacer height={pxToRem(10)} />
                </>
                )}

            <Slider {...settings}>
                {(loading &&
                    Array.from({ length: 7 }).map(
                      (item: IListMovieItem, index: number) => (
                            <CardMovie
                                image_url=""
                                id=""
                                key={index}
                                category=""
                                title=""
                                loading={true}
                                launch_date=""
                                episodes_counter=""
                                views=""
                            />
                      )
                    )) ||
                    items?.map((item: IListMovieItem) => {
                      return (
                            <CardMovie
                                category={category}
                                image_url={item.image_url}
                                key={item.id}
                                id={item.id}
                                title={item.title}
                                launch_date={item.launch_date}
                                episodes_counter={item.episodes_counter}
                                views={item.views}
                                teacher={item.teacher}
                            />
                      )
                    })}
            </Slider>
        </Container>
  )
}

export const CardMovie = (prop: IListMovieItem): JSX.Element => {
  const {
    id,
    title,
    image_url,
    launch_date,
    episodes_counter,
    views,
    category,
    loading,
    teacher
  } = prop
  const history = useNavigate()
  const handleMovie = (prop: any): any => {
    history(`/detail-movie/${category}/${id}`)
  }
  return (
        <CardMovieContainer onClick={handleMovie}>
            <a>
                {(!loading && <img src={image_url} />) || (
                    <Skeleton style={{ height: pxToRem(141) }} />
                )}
                {(!loading && <Text text={title} type={ETextType.h6} />) || (
                    <Skeleton style={{ height: pxToRem(47) }} />
                )}
                {(!loading && (
                    <Text
                        text={`${teacher ? `${teacher}` : ''} ${
                            launch_date ? `${launch_date}` : ''
                        } ${
                            episodes_counter
                                ? ` - ${episodes_counter}  episódios`
                                : ''
                        }${views ? `  - ${views} visualizações` : ''}`}
                        type={ETextType.p}
                    />
                )) || <Skeleton />}
            </a>
        </CardMovieContainer>
  )
}
