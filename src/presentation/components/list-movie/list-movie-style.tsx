import { jsToCss, pxToRem } from 'presentation/packages'
import styled, { css } from 'styled-components'
import { IProps } from './list-movie-interface'

export const Container = styled.div`
    max-width: calc(100vw - 61px);
    margin-left: 61px !important;
    width: 100%;
    margin: auto;
    h4 {
        margin-bottom: 10px;
        color: #fff;
        font-family: "Open Sans";
        font-style: normal;
        font-weight: 600;
        font-size: 20px;
        line-height: 27px;
    }
    .slick-prev {
        background-color: red;
    }
    .slick-next {
        background-color: red;
    }
    .slick-list {
        .slick-track {
            display: flex;
            width: auto !important;
            gap: 10px;
        }
        .slick-slide > div {
        }
        .slick-slide > div > div {
            width: 251px !important;
            color: white;
        }
    }

  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    max-width: 95%;
    flex: 0 auto;
    margin-left: 0 !important;
    padding: 8px;
    margin-left: 0 !important;
    ${(props: IProps) =>
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) =>
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};    
  }
`

export const CardMovieContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: ${pxToRem(251)} !important;
    height ${pxToRem(224)};
    transition: all 1s;
    grid-template-rows: 141px 56px 27px;
    cursor: pointer;
    h6{
        margin-left: 9px;
        margin-right: 9px;
        font-weight: 600;
        font-size: 13.5px;
        padding-top:10px;
        padding-bottom:29px;
        text-align: left;
    }
    p{
        margin-left: 9px;
        margin-right: 9px;
        color: #B4B8CE;
        text-align: left;
    }
    a{
        display: grid;
        grid-template-rows: 141px 56px 27px;
    }
    &:hover{
        transform: scale(0.9);
        transition: all 1s;

    }
`
