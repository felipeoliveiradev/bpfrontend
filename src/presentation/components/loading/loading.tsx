import React from 'react'
import Lottie from 'lottie-react'
import LoadingJson from './ loading.json'
import { ILoading } from './loading-interface'
import { Container } from './loading-style'

export const Loading = (prop: ILoading): JSX.Element => {
  const { props } = prop
  const options = {
    animationData: LoadingJson,
    loop: true,
    autoplay: true
  }
  return (
      <Container {...props}>
        <Lottie {...options}/>
      </Container>
  )
}
