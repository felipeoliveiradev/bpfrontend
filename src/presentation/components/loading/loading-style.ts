import { jsToCss, pxToRem } from 'presentation/packages'
import styled, { css } from 'styled-components'
import { IProps } from '../list-movie/list-movie-interface'

export const Container = styled.div`
  
  ${(props: IProps) => props.styles?.desktop && css`${jsToCss(props.styles?.desktop)}`};
  
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.tablet)}) {
    ${(props: IProps) =>
      props.styles?.tablet && css`${jsToCss(props.styles?.tablet)}`};
  }
  @media (max-width: ${(props: any) => pxToRem(props.theme.query?.cellphone)}) {
    ${(props: IProps) =>
      props.styles?.mobile && css`${jsToCss(props.styles?.mobile)}`};    
  }
`
