import { useAuth } from 'main/contexts'
import { Layout } from 'presentation/packages'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { layout } from './header-layout'
type HeaderType = {
  mobile: boolean
}
export const Header = ({ mobile }: HeaderType): JSX.Element => {
  const history = useNavigate()
  const { user } = useAuth()
  return <Layout {...layout({ mobile, history, user })} />
}
