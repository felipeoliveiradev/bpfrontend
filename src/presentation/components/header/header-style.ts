import { pxToRem } from 'presentation/packages'
import styled from 'styled-components'

export const Avatar = styled.div`
    margin: auto;
    flex: 1;
    display: flex;
    align-items: center;
    padding-right: ${pxToRem(58)};
    img {
        width: ${pxToRem(40)};
        height: ${pxToRem(40)};
        width: auto;
        border-radius: ${pxToRem(100)};
    }
`
export const Logo = styled.div`
    display: flex;
    align-items: center;
    flex: 1;
    margin-left: ${pxToRem(60)};
    img {
        width: ${pxToRem(46)};
        height: auto;
    }
`
