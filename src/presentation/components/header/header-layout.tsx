import {
  Column,
  Container,
  Line,
  Navbar,
  pxToRem
} from 'presentation/packages'
import React from 'react'
import { Avatar, Logo } from './header-style'
import {
  RiHome2Line,
  RiPlayCircleLine,
  RiMentalHealthLine,
  RiBaseStationLine
} from 'react-icons/ri'
import './header.css'
export const layout = (props: any): object => {
  const { mobile, history, user } = props
  let userInfo: string

  return {
    children: Container({
      children: [
        Line({
          children: [
            Column({
              children: [
                                <Logo key="logo">
                                    <img src="/images/logo2.svg" />
                                </Logo>
              ],
              props: {
                styles: {
                  desktop: {
                    maxWidth: pxToRem(147)
                  },
                  tablet: {
                    display: 'none'
                  }
                }
              }
            }),
            Column({
              children: [
                Navbar({
                  items: [
                    {
                      name: 'Início',
                      status: true,
                      onClick: () => {
                        history('/browse')
                      },
                      icon: {
                        element: <RiHome2Line />
                      }
                    },
                    {
                      name: 'Conteúdo',
                      status: true,
                      link: '/',
                      icon: {
                        element: <RiPlayCircleLine />
                      }
                    },
                    {
                      name: 'Formação',
                      status: true,
                      link: '/',
                      icon: {
                        element: <RiMentalHealthLine />
                      }
                    },
                    {
                      name: 'Lives',
                      status: true,
                      link: '/',
                      icon: {
                        element: <RiBaseStationLine />
                      }
                    }
                  ],
                  props: {
                    className: 'imageIcon',
                    styles: {
                      desktop: {
                        fontSize: pxToRem(14),
                        gap: pxToRem(20)
                      },
                      tablet: {
                        display: 'flex',
                        justifyContent: 'space-between',
                        maxWidth: '80%',
                        margin: 'auto',
                        fontSize: pxToRem(11),
                        borderTop: '1px solid #222329'
                      }
                    }
                  }
                })
              ],
              props: {
                styles: {
                  desktop: {
                    flex: 1
                  },
                  tablet: {
                    flex: 1,
                    justifyContent: 'center'
                  }
                }
              }
            }),
            Column({
              children: [
                                <Avatar key="avatar">
                                    <img src={user} />
                                </Avatar>
              ],
              props: {
                styles: {
                  desktop: {
                    maxWidth: pxToRem(115),
                    width: '100%'
                  },
                  tablet: {
                    display: 'none'
                  }
                }
              }
            })
          ],
          props: {
            styles: {
              desktop: {
                background: '#000',
                height: pxToRem(68)
              },
              tablet: {
                height: pxToRem(79)
              }
            }
          }
        })
      ],
      props: {
        styles: {
          desktop: { display: mobile ? 'none' : 'flex' },
          tablet: {
            display: mobile ? 'flex' : 'none',
            position: 'fixed',
            zIndex: 3,
            bottom: 0,
            width: '100%'
          }
        }
      }
    })
  }
}
