export class NotFoundError extends Error {
  constructor () {
    super('Não foi encontrado nenhum resultado para essa busca')
    this.name = 'NotFoundError'
  }
}
