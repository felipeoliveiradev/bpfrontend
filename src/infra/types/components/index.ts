import { NavigateFunction } from 'react-router-dom'

export interface IComponents<T, R> {
  values?: T
  state?: R
  history?: NavigateFunction
}
