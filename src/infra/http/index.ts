import axios, { AxiosResponse } from 'axios'
import { IHttpOutput } from './interface-http'
export default axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'Content-type': 'application/json',
    Authorization: sessionStorage.getItem('token') ? sessionStorage.getItem('token') : ''
  },
  validateStatus: function (status) {
    return status >= 200 && status < 500 // default
  }
})

export function HttpResponse<T> (props: AxiosResponse<T | any>): IHttpOutput<T> {
  return {
    status: props.data?.status,
    statusCode: props?.status,
    message: props.data?.message,
    body: props.data?.result
  }
}
