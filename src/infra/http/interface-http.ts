export interface IHttpOutput<R> {
  status?: boolean
  statusCode?: number
  message?: string
  body?: R
}
