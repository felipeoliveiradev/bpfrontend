const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
module.exports = {
    mode: "development",
    entry: "./src/main/index.tsx",
    output: {
        path: path.join(__dirname, "public/js"),
        publicPath: "./public/js",
        filename: "bundle.js",
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"],
        alias: {
            data: path.join(__dirname, "src/data"),
            presentation: path.join(__dirname, "src/presentation"),
            main: path.join(__dirname, "src/main"),
            domain: path.join(__dirname, "src/domain"),
            infra: path.join(__dirname, "src/infra"),
        },
    },
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
              },
        ],
    },
    devServer: {
        devMiddleware: {
            writeToDisk: true,
        },
        static: {
            directory: path.join(__dirname, "public"),
        },
        historyApiFallback: true,
        port: 8081,
    },
    // externals: {
    //     react: 'React',
    //     'react-dom': 'ReactDOM'
    // },
    plugins: [new CleanWebpackPlugin()],
};
