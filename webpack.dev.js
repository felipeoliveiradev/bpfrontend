const { DefinePlugin } = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");
const path = require("path");

module.exports = merge(common, {
    mode: "development",
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                loader: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    devtool: "inline-source-map",
    devServer: {
        devMiddleware: {
            writeToDisk: true,
        },
        static: {
            directory: path.join(__dirname, "public"),
        },
        historyApiFallback: true,
        port: 8081,
    },
    plugins: [
        new DefinePlugin({
            "process.env.API_URL": JSON.stringify(
                "https://brasilparalelo.tk/api"
            ),
        }),
        new HtmlWebpackPlugin({
            template: "./template.dev.html",
        }),
    ],
});
