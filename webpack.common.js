
   
const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
    entry: "./src/main/index.tsx",
    output: {
        path: path.join(__dirname, "public/js"),
        publicPath: "./public/js",
        filename: "bundle.js",
    },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.css'],
    alias: {
        data: path.join(__dirname, "src/data"),
        presentation: path.join(__dirname, "src/presentation"),
        main: path.join(__dirname, "src/main"),
        domain: path.join(__dirname, "src/domain"),
        infra: path.join(__dirname, "src/infra"),
    },
  },
  plugins: [
    new CleanWebpackPlugin()
  ]
}